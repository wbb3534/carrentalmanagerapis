package store.developer.seop.api.customer.info.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum RequestType {
    RENT("렌트")
    , LEASE("리스")
    , ETC("미정")

    ;
    private final String typeName;
}
