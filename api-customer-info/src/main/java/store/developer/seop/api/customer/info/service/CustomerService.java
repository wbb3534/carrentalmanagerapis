package store.developer.seop.api.customer.info.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import store.developer.seop.api.customer.info.entity.CustomerInfo;
import store.developer.seop.api.customer.info.model.CustomerItem;
import store.developer.seop.api.customer.info.model.CustomerRequest;
import store.developer.seop.api.customer.info.model.CustomerResponse;
import store.developer.seop.api.customer.info.repository.CustomerInfoRepository;
import store.developer.seop.common.exception.CMissingDataException;
import store.developer.seop.common.response.model.ListResult;
import store.developer.seop.common.response.service.ListConvertService;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class CustomerService {
    private final CustomerInfoRepository customerInfoRepository;

    public void setCustomerInfo(CustomerRequest request) {
        CustomerInfo addData = new CustomerInfo.CustomerInfoBuilder(request).build();

        customerInfoRepository.save(addData);
    }

    public ListResult<CustomerItem> getCustomersInfo() {
        List<CustomerInfo> originData = customerInfoRepository.findAll();

        List<CustomerItem> result = new LinkedList<>();

        originData.forEach(item -> result.add(new CustomerItem.CustomerItemBuilder(item).build()));

        return ListConvertService.settingResult(result);
    }
    public CustomerResponse getCustomerInfo(long id) {
        CustomerInfo originData = customerInfoRepository.findById(id).orElseThrow(CMissingDataException::new);

        return new CustomerResponse.CustomerResponseBuilder(originData).build();
    }
}
