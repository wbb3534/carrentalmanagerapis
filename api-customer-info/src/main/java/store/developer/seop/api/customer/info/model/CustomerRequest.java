package store.developer.seop.api.customer.info.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import store.developer.seop.api.customer.info.enums.RequestType;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;


@Getter
@Setter
public class CustomerRequest {
    @ApiModelProperty(notes = "이름(2~20글자)", required = true)
    @NotNull
    @Length(min = 2, max = 20)
    private String customerName;
    @ApiModelProperty(notes = "전화번호(13글자)", required = true)
    @NotNull
    @Length(min = 13, max = 13)
    private String customerPhone;
    @ApiModelProperty(notes = "상담유형", required = true)
    @NotNull
    @Enumerated(value = EnumType.STRING)
    private RequestType requestType;
}
