package store.developer.seop.api.customer.info.entity;


import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import store.developer.seop.api.customer.info.enums.RequestType;
import store.developer.seop.api.customer.info.model.CustomerRequest;
import store.developer.seop.common.interfaces.CommonModelBuilder;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class CustomerInfo {
    @ApiModelProperty(value = "시퀀스")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ApiModelProperty(value = "고객명")
    @Column(nullable = false, length = 20)
    private String customerName;
    @ApiModelProperty(value = "고객 번호")
    @Column(nullable = false, length = 13)
    private String customerPhone;
    @ApiModelProperty(value = "상담타입")
    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false, length = 5)
    private RequestType requestType;
    @ApiModelProperty(value = "상담요청 들어온 시간")
    @Column(nullable = false)
    private LocalDateTime requestTime;

    private CustomerInfo(CustomerInfoBuilder builder) {
        this.customerName = builder.customerName;
        this.customerPhone = builder.customerPhone;
        this.requestType = builder.requestType;
        this.requestTime = builder.requestTime;
    }
    public static class CustomerInfoBuilder implements CommonModelBuilder<CustomerInfo> {
        private final String customerName;
        private final String customerPhone;
        private final RequestType requestType;
        private final LocalDateTime requestTime;

        public CustomerInfoBuilder(CustomerRequest request) {
            this.customerName = request.getCustomerName();
            this.customerPhone = request.getCustomerPhone();
            this.requestType = request.getRequestType();
            this.requestTime = LocalDateTime.now();
        }
        @Override
        public CustomerInfo build() {
            return new CustomerInfo(this);
        }
    }
}
