package store.developer.seop.api.customer.info.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import store.developer.seop.api.customer.info.entity.CustomerInfo;
import store.developer.seop.common.interfaces.CommonModelBuilder;

import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class CustomerResponse {
    @ApiModelProperty(value = "시퀀스")
    private Long id;
    @ApiModelProperty(value = "고객명")
    private String customerName;
    @ApiModelProperty(value = "고객 번호")
    private String customerPhone;
    @ApiModelProperty(value = "상담타입")
    private String requestType;
    @ApiModelProperty(value = "상담요청 들어온 시간")
    private LocalDateTime requestTime;

    private CustomerResponse(CustomerResponseBuilder builder) {
        this.id = builder.id;
        this.customerName = builder.customerName;
        this.customerPhone = builder.customerPhone;
        this.requestType = builder.requestType;
        this.requestTime = builder.requestTime;
    }

    public static class CustomerResponseBuilder implements CommonModelBuilder<CustomerResponse> {
        private final Long id;
        private final String customerName;
        private final String customerPhone;
        private final String requestType;
        private final LocalDateTime requestTime;

        public CustomerResponseBuilder(CustomerInfo customerInfo) {
            this.id = customerInfo.getId();
            this.customerName = customerInfo.getCustomerName();
            this.customerPhone = customerInfo.getCustomerPhone();
            this.requestType = customerInfo.getRequestType().getTypeName();
            this.requestTime = customerInfo.getRequestTime();
        }

        @Override
        public CustomerResponse build() {
            return new CustomerResponse(this);
        }
    }
}
