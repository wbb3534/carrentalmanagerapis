package store.developer.seop.api.customer.info.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import store.developer.seop.api.customer.info.model.CustomerItem;
import store.developer.seop.api.customer.info.model.CustomerRequest;
import store.developer.seop.api.customer.info.model.CustomerResponse;
import store.developer.seop.api.customer.info.service.CustomerService;
import store.developer.seop.common.response.model.CommonResult;
import store.developer.seop.common.response.model.ListResult;
import store.developer.seop.common.response.model.SingleResult;
import store.developer.seop.common.response.service.ResponseService;

import javax.validation.Valid;

@Api(tags = "고객관리")
@RequestMapping("/v1/customer")
@RestController
@RequiredArgsConstructor
public class CustomerController {
    private final CustomerService customerService;
    @ApiOperation(value = "고객정보 등록")
    @PostMapping("/new")
    public CommonResult setCustomerInfo(@RequestBody @Valid CustomerRequest request) {
        customerService.setCustomerInfo(request);

        return ResponseService.getSuccessResult();
    }
    @ApiOperation(value = "고객정보 리스트")
    @GetMapping("/list")
    public ListResult<CustomerItem> getCustomersInfo() {
        return ResponseService.getListResult(customerService.getCustomersInfo(), true);
    }
    @ApiOperation(value = "고객정보 단수")
    @GetMapping("/data/{id}")
    public SingleResult<CustomerResponse> getCustomerInfo(@PathVariable long id) {
        return ResponseService.getSingleResult(customerService.getCustomerInfo(id));
    }
}
