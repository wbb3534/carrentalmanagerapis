package store.developer.seop.api.customer.info;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiCustomerInfoApplication {
    public static void main(String[] args) {
        SpringApplication.run(ApiCustomerInfoApplication.class, args);
    }
}
