package store.developer.seop.api.customer.info.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import store.developer.seop.api.customer.info.entity.CustomerInfo;

public interface CustomerInfoRepository extends JpaRepository<CustomerInfo, Long> {
}
