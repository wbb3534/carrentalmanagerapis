package store.developer.seop.api.customer.assignment.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import store.developer.seop.api.customer.assignment.model.AssignmentItem;
import store.developer.seop.api.customer.assignment.service.AssignmentService;
import store.developer.seop.common.response.model.CommonResult;
import store.developer.seop.common.response.model.ListResult;
import store.developer.seop.common.response.service.ResponseService;

@Api(tags = "고객 배분")
@RequiredArgsConstructor
@RestController
@RequestMapping("/v1/assignment")
public class AssignmentController {
    private final AssignmentService assignmentService;

    @ApiOperation(value = "고객배분 등록")
    @PostMapping("/new/{customerId}/{salesManId}")
    public CommonResult setAssignment(@PathVariable long customerId, @PathVariable long salesManId) {
        assignmentService.setAssignment(customerId, salesManId);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "고객배분 리스트")
    @GetMapping("/list")
    public ListResult<AssignmentItem> getCustomersInfo() {
        return ResponseService.getListResult(assignmentService.getAssignments(), true);
    }
}
