package store.developer.seop.api.customer.assignment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiCustomerAssignmentApplication {
    public static void main(String[] args) {
        SpringApplication.run(ApiCustomerAssignmentApplication.class, args);
    }
}
