package store.developer.seop.api.customer.assignment.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import store.developer.seop.api.customer.assignment.entity.Assignment;
import store.developer.seop.api.customer.assignment.model.AssignmentItem;
import store.developer.seop.api.customer.assignment.repository.AssignmentRepository;
import store.developer.seop.common.response.model.ListResult;
import store.developer.seop.common.response.service.ListConvertService;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class AssignmentService {
    private final AssignmentRepository assignmentRepository;

    public void setAssignment(long customerId, long salesManId) {
        Assignment addData = new Assignment.AssignmentBuilder(customerId, salesManId).build();

        assignmentRepository.save(addData);
    }

    public ListResult<AssignmentItem> getAssignments() {
        List<Assignment> originData = assignmentRepository.findAll();

        List<AssignmentItem> result = new LinkedList<>();

        originData.forEach(item -> result.add(new AssignmentItem.AssignmentItemBuilder(item).build()));

        return ListConvertService.settingResult(result);
    }
}
