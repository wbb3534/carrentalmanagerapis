package store.developer.seop.api.customer.assignment.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import store.developer.seop.api.customer.assignment.entity.Assignment;

public interface AssignmentRepository extends JpaRepository<Assignment, Long> {
}
