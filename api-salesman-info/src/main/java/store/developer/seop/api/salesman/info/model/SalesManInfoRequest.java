package store.developer.seop.api.salesman.info.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import store.developer.seop.api.salesman.info.enums.WorkDepartment;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
public class SalesManInfoRequest {
    @ApiModelProperty(notes = "이름(2~20글자)", required = true)
    @NotNull
    @Length(min = 2, max = 20)
    private String salesManName;
    @ApiModelProperty(notes = "전화번호(13글자)", required = true)
    @NotNull
    @Length(min = 13, max = 13)
    private String salesManPhone;
    @ApiModelProperty(value = "근무 부서")
    @NotNull
    @Enumerated(value = EnumType.STRING)
    private WorkDepartment workDepartment;
    @ApiModelProperty(value = "입사일")
    @NotNull
    private LocalDate joinCompanyDate;
}
