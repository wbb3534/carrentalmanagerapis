package store.developer.seop.api.salesman.info.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import store.developer.seop.api.salesman.info.enums.WorkDepartment;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class SalesManInfoUpdateRequest {
    @ApiModelProperty(value = "영업사원명")
    @NotNull
    @Length(min = 2, max = 20)
    private String salesManName;
    @ApiModelProperty(value = "영업사원 번호")
    @NotNull
    @Length(min = 13, max = 13)
    private String salesManPhone;
    @ApiModelProperty(value = "근무 부서")
    @NotNull
    @Enumerated(value = EnumType.STRING)
    private WorkDepartment workDepartment;
}
