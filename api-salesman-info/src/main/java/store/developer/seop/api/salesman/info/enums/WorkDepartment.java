package store.developer.seop.api.salesman.info.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum WorkDepartment {
    ANSAN("안산")
    , SEOUL("서울")
    , SUWON("수원")
    , CHEONAN("천안")

    ;

    private final String departmentName;
}
