package store.developer.seop.api.salesman.info.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import store.developer.seop.api.salesman.info.entity.SalesMan;
import store.developer.seop.common.interfaces.CommonModelBuilder;

import java.time.LocalDate;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class SalesManInfoResponse {
    @ApiModelProperty(value = "시퀀스")
    private Long id;
    @ApiModelProperty(value = "영업사원명")
    private String salesManName;
    @ApiModelProperty(value = "영업사원 번호")
    private String salesManPhone;
    @ApiModelProperty(value = "근무 부서")
    private String workDepartment;
    @ApiModelProperty(value = "입사일")
    private LocalDate joinCompanyDate;

    private SalesManInfoResponse(SalesManInfoResponseBuilder builder) {
        this.id = builder.id;
        this.salesManName = builder.salesManName;
        this.salesManPhone = builder.salesManPhone;
        this.workDepartment = builder.workDepartment;
        this.joinCompanyDate = builder.joinCompanyDate;
    }
    public static class SalesManInfoResponseBuilder implements CommonModelBuilder<SalesManInfoResponse> {
        private final Long id;
        private final String salesManName;
        private final String salesManPhone;
        private final String workDepartment;
        private final LocalDate joinCompanyDate;

        public SalesManInfoResponseBuilder(SalesMan salesMan) {
            this.id = salesMan.getId();
            this.salesManName = salesMan.getSalesManName();
            this.salesManPhone = salesMan.getSalesManPhone();
            this.workDepartment = salesMan.getWorkDepartment().getDepartmentName();
            this.joinCompanyDate = salesMan.getJoinCompanyDate();
        }
        @Override
        public SalesManInfoResponse build() {
            return new SalesManInfoResponse(this);
        }
    }
}
