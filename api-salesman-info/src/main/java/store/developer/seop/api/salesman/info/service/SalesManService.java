package store.developer.seop.api.salesman.info.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import store.developer.seop.api.salesman.info.entity.SalesMan;
import store.developer.seop.api.salesman.info.model.SalesManInfoItem;
import store.developer.seop.api.salesman.info.model.SalesManInfoRequest;
import store.developer.seop.api.salesman.info.model.SalesManInfoResponse;
import store.developer.seop.api.salesman.info.model.SalesManInfoUpdateRequest;
import store.developer.seop.api.salesman.info.repository.SalesManRepository;
import store.developer.seop.common.exception.CMissingDataException;
import store.developer.seop.common.response.model.ListResult;
import store.developer.seop.common.response.service.ListConvertService;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class SalesManService {
    private final SalesManRepository salesManRepository;

    public void setSalesManInfo(SalesManInfoRequest request) {
        SalesMan addData = new SalesMan.SalesManBuilder(request).build();

        salesManRepository.save(addData);
    }

    public ListResult<SalesManInfoItem> getSalesMansInfo() {
        List<SalesMan> originData = salesManRepository.findAll();
        List<SalesManInfoItem> result = new LinkedList<>();

        originData.forEach(item -> result.add(new SalesManInfoItem.SalesManInfoItemBuilder(item).build()));

        return ListConvertService.settingResult(result);
    }

    public SalesManInfoResponse getSalesManInfo(long id) {
        SalesMan origin = salesManRepository.findById(id).orElseThrow(CMissingDataException::new);

        return new SalesManInfoResponse.SalesManInfoResponseBuilder(origin).build();
    }

    public void putSalesManInfo(long id, SalesManInfoUpdateRequest request) {
        SalesMan salesMan = salesManRepository.findById(id).orElseThrow(CMissingDataException::new);
        salesMan.putSalesManInfo(request);

        salesManRepository.save(salesMan);

    }
}
