package store.developer.seop.api.salesman.info.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import store.developer.seop.api.salesman.info.entity.SalesMan;

public interface SalesManRepository extends JpaRepository<SalesMan, Long> {
}
