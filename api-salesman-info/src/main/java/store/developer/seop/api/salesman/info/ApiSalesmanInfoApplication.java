package store.developer.seop.api.salesman.info;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiSalesmanInfoApplication {
    public static void main(String[] args) {
        SpringApplication.run(ApiSalesmanInfoApplication.class, args);
    }
}
