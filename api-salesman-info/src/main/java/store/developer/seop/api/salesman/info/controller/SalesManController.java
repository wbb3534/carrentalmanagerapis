package store.developer.seop.api.salesman.info.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import store.developer.seop.api.salesman.info.model.SalesManInfoItem;
import store.developer.seop.api.salesman.info.model.SalesManInfoRequest;
import store.developer.seop.api.salesman.info.model.SalesManInfoResponse;
import store.developer.seop.api.salesman.info.model.SalesManInfoUpdateRequest;
import store.developer.seop.api.salesman.info.service.SalesManService;
import store.developer.seop.common.response.model.CommonResult;
import store.developer.seop.common.response.model.ListResult;
import store.developer.seop.common.response.model.SingleResult;
import store.developer.seop.common.response.service.ResponseService;

import javax.validation.Valid;

@Api(tags = "영업사원 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/salesman")
public class SalesManController {
    private final SalesManService salesManService;

    @ApiOperation(value = "영업사원 정보 등록")
    @PostMapping("/new")
    public CommonResult setSalesManInfo(@RequestBody @Valid SalesManInfoRequest request) {
        salesManService.setSalesManInfo(request);

        return ResponseService.getSuccessResult();
    }
    @ApiOperation(value = "영업사원 정보 리스트")
    @GetMapping("/list")
    public ListResult<SalesManInfoItem> getSalesMansInfo() {
        return ResponseService.getListResult(salesManService.getSalesMansInfo(), true);
    }

    @ApiOperation(value = "영업사원 정보 단수")
    @GetMapping("/data/{id}")
    public SingleResult<SalesManInfoResponse> getSalesManInfo(@PathVariable long id) {
        return ResponseService.getSingleResult(salesManService.getSalesManInfo(id));
    }
    @ApiOperation(value = "영업사원 정보 수정")
    @PutMapping("/info/{id}")
    public CommonResult putSalesManInfo(@PathVariable long id, @RequestBody @Valid SalesManInfoUpdateRequest request) {
        salesManService.putSalesManInfo(id, request);

        return ResponseService.getSuccessResult();
    }
}
